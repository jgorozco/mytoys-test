# README #

This readme had some details about this interview test. 
### Warning! ###

After all,i had a couple of interruptions about external stuff and spend much more time that i expected. 
I had a small problem with WebView and this still me much time and mind peacefull. 

### Details about the code ###

It was only an activity, there is no fragments but custom views. 

In Core folder is all the managing stuff. The usecase _UseCaseGetData_ (I prefer do not use Dagger for a  such small project because in my experience, sometimes configure it well need more time that the time that save). But the Structure had dependency inyection by constructor (The old way to do this!)

In android folder are located all the android classes. The main component, webview, is inside a custom view because had all the init configuration, loading and (in the future) animations, flow control for webview and this kind of things you could do if you have more time.

The list view is a custom view too, This custom view has the responsabillity to retrieve the data and paint it, and manage the login. My point is, every view had to manage all the stuff to pain itself. I use a Callback interface (such as android does with his view components) to interact with _external wold_, i mean, other views or classes

### About Test and Clean arquitecture ###
I Cheat in a place about it, The UseCase _getDataFromServer_ need on _Retrofit_ library. Retrofit is an adapter and this is NOT clean arquitecture. I will need to wrarp this Callback into a generic callback, creating a _OnSuccess_ and a _OnError_ with my own classes. But in this case i consider that it could be _refactor_ in the future to be more accurate.

* About testing, The UseCase only is a wrarp and using Retrofit i dont need to manage this part, there is no business to test in this part of code. 
* POJOS could be tested but i dont think is really necessary.
*Class to be tested will be _MtListNavigationView_ that will receive a MOCK DataAdapter and will make some android test based on it, for example check if receive an error, an object without format... but to be really tested we must create a _presenter_ that receive the events and manage it to show to the view. I dont think about it until was already done, and if i implement it will be only a wrapper.
*About UX testing, we could use Espresso and we could test the interaction between ListView and WebView. 
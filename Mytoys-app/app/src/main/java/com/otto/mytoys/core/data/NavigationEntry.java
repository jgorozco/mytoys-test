package com.otto.mytoys.core.data;

/**
 * Created by josegarciaorozco on 25/8/16.
 */
public class NavigationEntry {
    private NavigationEntries[] navigationEntries;

    public NavigationEntries[] getNavigationEntries ()
    {
        return navigationEntries;
    }

    public void setNavigationEntries (NavigationEntries[] navigationEntries)
    {
        this.navigationEntries = navigationEntries;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [navigationEntries = "+navigationEntries+"]";
    }
}

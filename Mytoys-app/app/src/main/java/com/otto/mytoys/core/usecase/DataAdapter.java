package com.otto.mytoys.core.usecase;

import com.otto.mytoys.core.data.NavigationEntry;

import retrofit.Callback;

/**
 * Created by josegarciaorozco on 25/8/16.
 */
public interface DataAdapter {

    void getNavData(Callback<NavigationEntry> cb);
}

package com.otto.mytoys.core.adapter;

import com.otto.mytoys.Constants;
import com.otto.mytoys.core.data.NavigationEntry;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Headers;


/**
 * Created by josegarciaorozco on 25/8/16.
 */
public interface MytoysEndpoint {

    @GET("/api/navigation")
    @Headers({Constants.MYTOYS_HEADER})
    void GetNavigationData( Callback<NavigationEntry> cb);

}

package com.otto.mytoys;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;

import com.otto.mytoys.android.uxcomponent.MtListNavigationView;
import com.otto.mytoys.android.uxcomponent.MtWebView;
import com.otto.mytoys.core.adapter.GetDataFromMytoysImpl;

public class BaseActivity extends AppCompatActivity {

    private MtListNavigationView listView;
    private DrawerLayout mDrawerLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        final MtWebView webView= (MtWebView) findViewById(R.id.content_frame);
        listView= (MtListNavigationView) findViewById(R.id.BaseActivity_leftMenu);
        listView.initData(new GetDataFromMytoysImpl());
        listView.setBackListener(new MtListNavigationView.ListNavigationListener() {
            @Override
            public void onBackPressed() {
                mDrawerLayout.closeDrawers();
            }

            @Override
            public void loadUrl(String url) {
                webView.loadUrl(url);
                mDrawerLayout.closeDrawers();

            }
        });
        //this is not totally necesary, but users are used to use it...
        webView.initPullDownRefresh(this);
        configureToolBar();
        webView.loadUrl("https://www.mytoys.de");
    }

    private void configureToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.BaseActivity_full_screen_bar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,toolbar ,  android.R.string.ok, android.R.string.cancel) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //getActionBar().setTitle(mTitle);
                //invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getActionBar().setTitle(mDrawerTitle);
                //invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };


        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();


    }
}

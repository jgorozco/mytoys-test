package com.otto.mytoys.android.uxcomponent;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.otto.mytoys.R;

/**
 * Created by josegarciaorozco on 25/8/16.
 */
public class HeaderCellView extends LinearLayout {

    private ImageButton mLeftButton;
    private ImageButton mRightButton;
    private TextView mTextView;
    private OnHeaderClickListener mHeaderListener;

    public HeaderCellView(Context context) {
        super(context);
        initViews(context);
    }

    public HeaderCellView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
    }

    public HeaderCellView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context);
    }

    private void initViews(Context context) {
        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mInflater.inflate(R.layout.mtnavheader, this);
        mLeftButton= (ImageButton) findViewById(R.id.mtnavcell_buttonLeft);

        mRightButton= (ImageButton) findViewById(R.id.mtnavcell_buttonRight);
        mTextView= (TextView) findViewById(R.id.mtnavcell_text);
        mLeftButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mHeaderListener!=null){
                    mHeaderListener.OnClickBackButton();
                }
            }
        });
        mRightButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mHeaderListener!=null){
                    mHeaderListener.OnClickCloseButton();
                }
            }
        });
    }

    public void setHeaderListener(OnHeaderClickListener mHeaderListener) {
        this.mHeaderListener = mHeaderListener;
    }


    public void setHeaderContent(String title){
        if (TextUtils.isEmpty(title)){
            mTextView.setVisibility(GONE);
            mLeftButton.setImageResource(R.mipmap.mytoys_logo);

        }else{
            mTextView.setVisibility(VISIBLE);
            mTextView.setText(title);
            mLeftButton.setImageResource(android.R.drawable.ic_media_previous);
        }
    }

    public interface OnHeaderClickListener{
        void OnClickBackButton();
        void OnClickCloseButton();
    }
}

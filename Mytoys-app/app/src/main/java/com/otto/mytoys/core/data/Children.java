package com.otto.mytoys.core.data;

/**
 * Created by josegarciaorozco on 25/8/16.
 */
public class Children {
    private String label;

    private String type;

    private String url;

    private Children[] children;

    public String getLabel ()
    {
        return label;
    }

    public void setLabel (String label)
    {
        this.label = label;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [label = "+label+", type = "+type+", url = "+url+"]";
    }

    public Children[] getChildren() {
        return children;
    }

    public void setChildren(Children[] children) {
        this.children = children;
    }
}

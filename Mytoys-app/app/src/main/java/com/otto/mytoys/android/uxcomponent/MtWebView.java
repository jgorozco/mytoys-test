package com.otto.mytoys.android.uxcomponent;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.otto.mytoys.Constants;
import com.otto.mytoys.R;

import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

/**
 * Represent the webview component, with loading,
 * animation and all the stuff that this view/UXusecase will manage
 */
public class MtWebView extends RelativeLayout {
    private Context mContext;
    private ProgressBar mProgressBar;
    private WebView mWebView;
    private String mUrl="";

    public MtWebView(Context context) {
        super(context);
        initViews(context);
    }

    public MtWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
    }


    public MtWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context);
    }


    private void initViews(Context context) {
        mContext=context;
        LayoutInflater  mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mInflater.inflate(R.layout.mtwebview, this);
        mProgressBar= (ProgressBar) findViewById(R.id.mtwebview_progressbar);
        mProgressBar.setVisibility(GONE);
        mWebView= (WebView) findViewById(R.id.mtwebview_webview_content);
        initWebView();
    }

    public void initPullDownRefresh(Activity activity){
        final PullToRefreshLayout mPullToRefreshLayout = (PullToRefreshLayout) findViewById(R.id.ptr_layout);
        ActionBarPullToRefresh.from(activity)
                .allChildrenArePullable()
                .listener(new OnRefreshListener() {
                    @Override
                    public void onRefreshStarted(View view) {
                        loadStoredUrl(mUrl);
                        mPullToRefreshLayout.setRefreshComplete();
                    }
                })
        .setup(mPullToRefreshLayout);

    }

    private void initWebView() {
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                mProgressBar.setProgress(progress);
                if (progress==100) {
                    mProgressBar.setVisibility(GONE);
                }
                //TODO when progress was 100% if apply show animation
            }
        });
        mWebView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(mContext, description, Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * Create the loadurl method inside out custom view with loader and webview configuration
     * @param url url to load
     */
    public void loadUrl(String url){
        mUrl=url;
        mProgressBar.setVisibility(VISIBLE);
        mProgressBar.setProgress(1);
        loadStoredUrl(mUrl);
    }

    private void loadStoredUrl(String mUrl) {
        if (TextUtils.isEmpty(mUrl)){
            mUrl= Constants.MYTOYS_DE;
        }
        //TODO create an animation to hide the webview until was totally loaded if necesary
        mWebView.loadUrl(mUrl);
    }
}

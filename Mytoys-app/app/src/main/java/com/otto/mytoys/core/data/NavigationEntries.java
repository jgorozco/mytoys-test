package com.otto.mytoys.core.data;

/**
 * Created by josegarciaorozco on 25/8/16.
 */
public class NavigationEntries {
    private Children[] children;

    private String label;

    private String type;

    public Children[] getChildren ()
    {
        return children;
    }

    public void setChildren (Children[] children)
    {
        this.children = children;
    }

    public String getLabel ()
    {
        return label;
    }

    public void setLabel (String label)
    {
        this.label = label;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [children = "+children+", label = "+label+", type = "+type+"]";
    }
}

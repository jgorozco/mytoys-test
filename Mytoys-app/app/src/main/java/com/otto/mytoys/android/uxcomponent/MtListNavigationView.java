package com.otto.mytoys.android.uxcomponent;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.otto.mytoys.R;
import com.otto.mytoys.core.data.Children;
import com.otto.mytoys.core.data.NavigationEntries;
import com.otto.mytoys.core.data.NavigationEntry;
import com.otto.mytoys.core.usecase.DataAdapter;
import com.otto.mytoys.core.usecase.UseCaseGetData;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by josegarciaorozco on 25/8/16.
 */
public class MtListNavigationView extends LinearLayout {

    private ListView mList;
    private ProgressBar mProgress;
    private ListViewAdapter mAdapter;
    private HeaderCellView mHeader;
    private ListNavigationListener backListener;
    private ArrayList<Object> historyNavigation;
    private NavigationEntry baseNavEntry;
    private DataAdapter mRetrofitAdapter;

    public MtListNavigationView(Context context) {
        super(context);
        initViews(context);
    }


    public MtListNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
    }

    public MtListNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context);
    }

    private void initViews(Context context) {
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mInflater.inflate(R.layout.mtlistview, this);
        historyNavigation = new ArrayList<>();
        mHeader = new HeaderCellView(context);
        mAdapter = new ListViewAdapter(context);
        mList = (ListView) findViewById(R.id.mtlistview_list);
        mProgress = (ProgressBar) findViewById(R.id.mtlistview_progressbar);
        mList.setAdapter(mAdapter);

        mList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                manageClickListener(position);
                return false;
            }
        });
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                manageClickListener(position);
            }
        });
        mHeader.setHeaderContent("");
        mHeader.setHeaderListener(new HeaderCellView.OnHeaderClickListener() {
            @Override
            public void OnClickBackButton() {
                navigateBack();
            }

            @Override
            public void OnClickCloseButton() {
                if (backListener != null) {
                    backListener.onBackPressed();
                }

            }
        });
        mList.addHeaderView(mHeader);
    }

    /**
     * Init the component with the data
     * @param dataAdapter data adapter
     */
    public void initData(DataAdapter dataAdapter){
        mRetrofitAdapter=dataAdapter;
        initListViewContent();
    }

    private void navigateBack() {
        if (historyNavigation.size() > 1) {
            historyNavigation.remove(historyNavigation.size()-1);
            Object lastChild = historyNavigation.get(historyNavigation.size()-1);
            navigateToChild(lastChild);
            if (lastChild instanceof Children) {
                mHeader.setHeaderContent(((Children) lastChild).getLabel());
            } else {
                mHeader.setHeaderContent(((NavigationEntries) lastChild).getLabel());
            }
        } else {
            historyNavigation.clear();
            if (baseNavEntry==null){
                //if user click again and there is not content, try to get it again
                initListViewContent();
            }
            setAllData(baseNavEntry);
            mHeader.setHeaderContent("");
        }
    }

    private void manageClickListener(int position) {
        Object selectedObject = mAdapter.getItem(position);
        if (selectedObject instanceof Children) {
            Children childObject = (Children) selectedObject;
            if (!TextUtils.isEmpty(childObject.getUrl())) {
                if (backListener != null) {
                    backListener.loadUrl(childObject.getUrl());
                }
            } else {
                if (childObject.getChildren().length > 0) {
                    historyNavigation.add(childObject);
                    navigateToChild(childObject);
                }
            }
        } else if (selectedObject instanceof NavigationEntries) {
            NavigationEntries childObject = (NavigationEntries) selectedObject;
            if (childObject.getChildren().length > 0) {
                historyNavigation.add(childObject);
                navigateToChild(childObject);
            }
        }
    }
    private void navigateToChild(Object childObject) {
        if (childObject instanceof Children) {
            mHeader.setHeaderContent(((Children)childObject).getLabel());

            mAdapter.setList(new ArrayList<>(Arrays.asList(((Children)childObject).getChildren())));

        }else{
            mHeader.setHeaderContent(((NavigationEntries)childObject).getLabel());
            mAdapter.setList(new ArrayList<>(Arrays.asList(((NavigationEntries)childObject).getChildren())));
        }
    }

    private void setAllData(NavigationEntry body) {
        ArrayList initList=new ArrayList();
        if ((body!=null)&&(body.getNavigationEntries()!=null)) {
            for (NavigationEntries entity : body.getNavigationEntries()) {
                initList.add(entity);
                if (entity.getChildren() != null) {
                    for (Children child : entity.getChildren()) {
                        initList.add(child);
                    }

                }
            }
        }
        mAdapter.setList(initList);
    }

    private void initListViewContent() {

        mProgress.setVisibility(VISIBLE);
        UseCaseGetData.getDataFromServer(mRetrofitAdapter, new Callback<NavigationEntry>() {
            @Override
            public void success(NavigationEntry navigationEntry, Response response) {
                baseNavEntry=navigationEntry;
                setAllData(navigationEntry);
                mProgress.setVisibility(GONE);
            }

            @Override
            public void failure(RetrofitError error) {
                mProgress.setVisibility(GONE);
                Toast.makeText(getContext(),"Something wrong:"+error.getLocalizedMessage(),Toast.LENGTH_LONG).show();
            }

        });
    }



    public void setBackListener(ListNavigationListener backListener) {
        this.backListener = backListener;
    }


    public interface ListNavigationListener{
        void onBackPressed();
        void loadUrl(String url);
    }
}

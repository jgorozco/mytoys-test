package com.otto.mytoys.core.adapter;

import com.otto.mytoys.Constants;
import com.otto.mytoys.core.data.NavigationEntry;
import com.otto.mytoys.core.usecase.DataAdapter;

import retrofit.Callback;
import retrofit.RestAdapter;


/**
 * Created by josegarciaorozco on 25/8/16.
 */


public class GetDataFromMytoysImpl implements DataAdapter {

    RestAdapter restAdapter;
    public GetDataFromMytoysImpl() {
        restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.MYTOYS_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

    }

    @Override
    public void getNavData(Callback<NavigationEntry> cb) {
        restAdapter.create(MytoysEndpoint.class).GetNavigationData(cb);
    }
}

package com.otto.mytoys.android.uxcomponent;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.otto.mytoys.R;
import com.otto.mytoys.core.data.Children;
import com.otto.mytoys.core.data.NavigationEntries;

import java.util.ArrayList;

/**
 * Created by josegarciaorozco on 25/8/16.
 */
public class ListViewAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;
    private ArrayList<Object> mListObjects=new ArrayList<>();


    public ListViewAdapter(Context context){
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setList(ArrayList listObjects){
        mListObjects=listObjects;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (mListObjects!=null) {
            return mListObjects.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (mListObjects!=null) {
            return mListObjects.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            convertView=mInflater.inflate(R.layout.mtnavcell,null);
        }
        if (mListObjects!=null){
            Object actualNavObject=mListObjects.get(position);
            if (actualNavObject!=null){
                fillValuesIntoCell(convertView,actualNavObject);
            }
        }
        return convertView;
    }

    private void fillValuesIntoCell(View convertView, Object actualNavObject) {
        if (actualNavObject instanceof NavigationEntries){
            NavigationEntries entity= (NavigationEntries) actualNavObject;
            fillCellWith(convertView,entity.getLabel(),entity.getChildren().length>0,true);
        }else if (actualNavObject instanceof Children){
            Children entity= (Children) actualNavObject;
            fillCellWith(convertView,entity.getLabel(),entity.getChildren()==null,false);
        }else{
            fillCellWith(convertView,"INVALID DATA",false,false);
        }
    }

    private void fillCellWith(View convertView, String label, boolean needButton,boolean isseccion) {

        ((TextView) convertView.findViewById(R.id.mtnavcell_text)).setText(label);
        if (needButton) {
            convertView.findViewById(R.id.mtnavcell_image).setVisibility(View.GONE);
        }else{
            convertView.findViewById(R.id.mtnavcell_image).setVisibility(View.VISIBLE);
        }
        if (isseccion){
            convertView.findViewById(R.id.mtnavcell_cell).setBackgroundColor(Color.LTGRAY);
        }else{
            convertView.findViewById(R.id.mtnavcell_cell).setBackgroundColor(Color.WHITE);

        }
    }
}
